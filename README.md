# Time Tracker Backend

# Task API Node.js

Die vorliegende REST-Schnittstelle auf Basis von Node.js, express und MongoDB

## Vorraussetzungen

- [Node.js](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/download-center#community)

## Installation

1. Zur Installation in einem Terminal wie folgt vorgehen:

```bash
cd /path/to/project
npm install
```

## Ausführung

```bash
cd /path/to/project
npm start
```

## Testen
```bash
cd /path/to/project
npm test
```
