// Loading the configuration files under /config
const config = require('config');

// Creating a new express server
const express = require('express');
const app = express();

// Setting up and connecting to the database with Promises
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(config.db);

// Configuring the body parser to parse application/json, application/x-www-urlencoded and raw text
const bodyParser = require('body-parser');
const cors = require('cors');
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.text());

// Answering 'Hello World' on base path
app.get('/', (req, res) => {
    res.send('Hello World');
});


// Registering the task routes under /api
const taskRoute = require('./routes/task');
app.use('/api', taskRoute);

// Starting the server and listening under given config port
app.listen(config.port, () => {
    console.log('Server up and running');
});

module.exports = app;
