const express = require('express');
const router = express.Router();

const taskModel = require('./../model/task');

/**
 * GET /task route to retrieve all stored tasks
 * @param req
 * @param res
 */
const getTasks = (req, res) => {
    taskModel.find()
        .then(tasks => {
            res.json(tasks);
        })
        .catch(err => {
            res.send(err);
        });
};

/**
 * GET /task/:id route to retrieve a single task by id
 * @param req
 * @param res
 */
const getTask = (req, res) => {
    taskModel.findById(req.params.id)
        .then(task => {
            res.json(task)
        })
        .catch(err => {
            res.send(err);
        });
};


/**
 * POST /task route to create a new task
 *
 * @param req
 * @param res
 */
const postTasks = (req, res) => {
    const newTask = new taskModel(req.body);
    newTask.save()
        .then(task => {
            res.json(task)
        })
        .catch(err => {
            res.status(400);
            res.send(err);
        })
};

/**
 * DELETE /task/:id route to delete a single task by id
 *
 * @param req
 * @param res
 */
const deleteTask = (req, res) => {
    taskModel.findByIdAndRemove(req.params.id)
        .then(() => {
            res.status(204);
            res.send('');
        })
        .catch(err => res.send(err));
};

/**
 * PATCH /task/:id route to update a single task by id
 *
 * @param req
 * @param res
 */
const patchTask = (req, res) => {
    taskModel.findById(req.params.id)
        .then(task => {
            Object.assign(task, req.body).save()
                .then(task => {
                    res.json(task);
                })
                .catch(err => {
                    res.status(400);
                    res.send(err);
                })
        })
        .catch(err => {
            res.status(400);
            res.send(err);
        });
};

router.route('/task')
    .get(getTasks)
    .post(postTasks);

router.route('/task/:id')
    .get(getTask)
    .delete(deleteTask)
    .patch(patchTask);

module.exports = router;
