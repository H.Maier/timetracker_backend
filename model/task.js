const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Defining schema for the model Task
const TaskSchema = new Schema({
    description: {
        type: String,
        required: true
    },
    trackedTime: {
        type: Number,
        required: true
    },
    bookingDate: {
        type: Number,
        required: true

    }
});

// Registering a model Task with given mongoose schema
module.exports = mongoose.model('Task', TaskSchema);
