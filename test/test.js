// this will force the config module to load config/test.json
process.env.NODE_ENV = 'test';


const mongoose = require("mongoose");
const taskModel = require('../model/task');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

chai.use(chaiHttp);

// Describing our routine for tasks
describe('Tasks', function(){

    // clear database for each run through 'Tasks'
    beforeEach(done => {
        taskModel.remove({}, () => {
            done();
        });
    });

    /*
     * Test the /GET/ route
     */
    describe('/GET task', () => {
        // GET Task should return an empty array
        it('should GET all the tasks', done => {
            chai.request(server)
                .get('/api/task')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });

    /*
     * Test the /DELETE/:id route
     */
    describe('/DELETE task/:id ', () => {
        it('it should DELETE a task given the id', (done) => {
            const task = new taskModel({description: "Essen kaufen", trackedTime: 10000, bookingDate: Date.now()});
            task.save((err, task) => {
                chai.request(server)
                    .delete('/api/task/' + task.id)
                    .end((err, res) => {
                        res.should.have.status(204);
                        done();
                    });
            });
        });
    });

    /*
     * Test the /POST/ route
     */
    describe('/POST task', () => {
        // Functional test
        it('it should POST a correct task ', done => {
            const task = {
                description: "Essen kaufen",
                trackedTime: 10000,
                bookingDate: Date.now()
            };
            chai.request(server)
                .post('/api/task')
                .send(task)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('description');
                    res.body.should.have.property('trackedTime');
                    res.body.should.have.property('bookingDate');
                    done();
                });
        });

        // Dysfunctional test to ensure a task without title is not a valid task
        it('should not POST a task without description field', done => {
            const task = {
                trackedTime: 10000,
                bookingDate: Date.now()
            };
            chai.request(server)
                .post('/api/task')
                .send(task)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('description');
                    res.body.errors.description.should.have.property('kind').eql('required');
                    done();
                });
        });
    });

    /*
     * Test the /GET/:id route
     */
    describe('/GET task/:id ', () => {

        it('it should GET a task by the given id', (done) => {
            const task = new taskModel({description: "Essen kaufen", trackedTime: 10000, bookingDate: Date.now()});
            task.save((err, task) => {
                chai.request(server)
                    .get('/api/task/' + task.id)
                    .send(task)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('description');
                        res.body.should.have.property('trackedTime');
                        res.body.should.have.property('bookingDate');
                        res.body.should.have.property('_id').eql(task.id);
                        done();
                    });
            });

        });
    });

    /*
     * Test the /PATCH task/:id route
     */
    describe('/PATCH task/:id', () => {
        it('it should UPDATE a task given the id', (done) => {
            const task = new taskModel({description: "Essen kaufen", trackedTime: 10000, bookingDate: Date.now()});
            task.save((err, task) => {
                chai.request(server)
                    .patch('/api/task/' + task.id)
                    .send({description: "Vorlesung besuchen", trackedTime: 10000, bookingDate: Date.now()})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('description').eql("Vorlesung besuchen");
                        done();
                    });
            });
        });
    });

});
